# README #

* Based on alpine OS.
* Has all the tools needed to obfuscate, compress and to deliver a single python binary used for small footprint computers like BBB.
* Tools for static code analysis also included.
* Used in production CI.
* Container is hosted on docker hub.
* docker pull bosolsen/alping-python-app-builder

## TODO ##

* create tag with special python modules taken from the requirements.txt.
